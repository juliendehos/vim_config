" Use Vim settings, rather then Vi settings. 
" This setting must be as early as possible, as it has side effects.
set nocompatible

" Leader - ( Spacebar )
let mapleader = " "

" display options
set t_Co=256
set gfn=Monospace\ 13
syntax on
"colorscheme ron
"colorscheme Tomorrow-Night
colorscheme molokai
let g:molokai_original = 1
let g:rehash256 = 1

set number
set cursorline
set colorcolumn=80
set ruler         " show the cursor position all the time
set showcmd       " display incomplete command

" search
set hlsearch
set incsearch     " do incremental searching
set ignorecase    " case insensitive searching (unless specified)
set smartcase

" backup and undo
set undofile
set history=100

" completion
set wildmode=longest:full,full
set wildmenu

" mouse and scrolling
set mouse=a
set scrolloff=8 

" tabulation
set tabstop=2
set expandtab
set shiftwidth=2

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" HTML Editing
set matchpairs+=<:>

" Quicker window movement
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Always use vertical diffs
set diffopt+=vertical

" resize panes
nnoremap <silent> <Right> :vertical resize +5<cr>
nnoremap <silent> <Left> :vertical resize -5<cr>
nnoremap <silent> <Up> :resize +5<cr>
nnoremap <silent> <Down> :resize -5<cr>
 
" navigate through compilation errors
nmap <F4> :cp<cr>
nmap <F5> :cn<cr>

" spell checking
map <silent> <F10> "<Esc>:silent setlocal spell! spelllang=fr<CR>"
map <silent> <F11> "<Esc>:silent setlocal spell! spelllang=en<CR>"
nnoremap <F8> [s
nnoremap <F9> ]s
nnoremap <F1> z=

" delete buffer without losing the split window
nnoremap <C-c> :bp\|bd #<CR>


" -- plugins -- "

" init plugins using vundle 
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" YouCompleteMe
Plugin 'Valloric/YouCompleteMe'
let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_extra_conf.py'

" airline
Plugin 'vim-airline/vim-airline'
set laststatus=2

" nerdtree
Plugin 'scrooloose/nerdtree'
nmap <F6> :NERDTreeToggle<CR>

" tagbar
Plugin 'majutsushi/tagbar'
nmap <F7> :TagbarToggle<CR>

" vim-buffergator
" nmap <F2> gb
" nmap <F3> gB

"minibufexpl
Plugin 'fholgado/minibufexpl.vim'
let g:miniBufExplorerAutoStart = 1
nmap <F2> :MBEbp<cr>
nmap <F3> :MBEbn<cr>

" julia
Plugin 'JuliaEditorSupport/julia-vim'

" " fugitive
" Plugin 'tpope/vim-fugitive'
" set statusline+=%{fugitive#statusline()}

" " unite
" Plugin 'Shougo/vimproc.vim' 
" " make -C ~/.vim/bundle/vimproc.vim/
" Plugin 'Shougo/unite.vim'
" nnoremap <C-p> :Unite -auto-preview file_rec/async<cr>
" nnoremap <space>/ :Unite -auto-preview grep:.<cr>
" let g:unite_source_history_yank_enable = 1
" nnoremap <space>y :Unite history/yank<cr>
" nnoremap <space>s :Unite -quick-match buffer<cr>

" other plugins
Plugin 'easymotion/vim-easymotion'
Plugin 'lnl7/vim-nix'
" Plugin 'scrooloose/syntastic'
" Plugin 'vim-misc'

" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on

