# vim config

![](doc/screenshot3.png)

![](doc/screenshot2.png)

## plugins

- [vundle](https://github.com/VundleVim/Vundle.vim): plug-in manager
- [youcompleteme](https://valloric.github.io/YouCompleteMe/): code-completion engine
- [airline](https://github.com/vim-airline/vim-airline): status/tabline
- [nerdtree](https://github.com/scrooloose/nerdtree): tree explorer
- [tagbar](https://github.com/majutsushi/tagbar): displays tags in a window
- [minibufexpl](https://github.com/fholgado/minibufexpl.vim): buffer explorer
- [easymotion](https://github.com/easymotion/vim-easymotion): jump to targets
- [vim-buffkill](https://github.com/qpkorr/vim-bufkill): close a buffer without closing the window


## installation

- install curl, python, clang, llvm, and vim (with python support)
- `curl https://raw.githubusercontent.com/juliendehos/vim_config/master/install.sh | sh`

## key-bindings (edit mode)

| keys                        | action                                         |
|-----------------------------|------------------------------------------------|
| `<f2>`, `<f3>`              | previous/next buffer                           |
| `<f4>`, `<f5>`              | previous/next compilation error                |
| `<f6>`                      | open/close nerdtree                            |
| `<f7>`                      | open/close tagbar                              |
| `<f10>`, `<f11>`            | spell checking fr/en                           |
| `<f8>`, `<f9>`              | previous/next spelling error                   |
| `<f1>`                      | suggest spelling correction                    |
| `C-c`                       | close buffer inside pane                       |
| `<space> <space> w`         | easy motion                                    |
| `<space> <space> e`         | easy motion (end of word)                      |
| arrows                      | resize current pane                            |
| `c-h`, `c-j`, `c-k`, `c-l`  | change current pane                            |

