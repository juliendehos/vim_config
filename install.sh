#!/bin/sh
rm -rf ~/.vim*
curl https://raw.githubusercontent.com/juliendehos/vim_config/master/.vim/colors/molokai.vim -o ~/.vim/colors/molokai.vim --create-dirs
curl https://raw.githubusercontent.com/juliendehos/vim_config/master/.vim/ycm_extra_conf.py -o ~/.vim/ycm_extra_conf.py
curl https://raw.githubusercontent.com/juliendehos/vim_config/master/.vimrc -o ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim -c PluginInstall -c qa!
cd ~/.vim/bundle/YouCompleteMe
./install.py
curl http://ftp.vim.org/vim/runtime/spell/fr.utf-8.spl -o ~/.vim/spell/fr.utf-8.spl --create-dirs
curl http://ftp.vim.org/vim/runtime/spell/fr.utf-8.sug -o ~/.vim/spell/fr.utf-8.sug 
curl http://ftp.vim.org/vim/runtime/spell/fr.latin1.spl -o ~/.vim/spell/fr.latin1.spl
curl http://ftp.vim.org/vim/runtime/spell/fr.latin1.sug -o ~/.vim/spell/fr.latin1.sug 

